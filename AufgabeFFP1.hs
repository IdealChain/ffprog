-- | Solution for FFP Aufgabe 1
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP1 where

-- | Stream of powers of two
pof2s :: [Integer]
pof2s = 1 : map (*2) pof2s

-- | Stream of pascal tuples
pd :: [[Integer]]
pd = iterate nextRow [1]
   where nextRow row = zipWith (+) ([0] ++ row) (row ++ [0])

-- | List of diagonal summands of pd
fibdiag :: Integer -> [Integer]
fibdiag n = [ pd !! (fromIntegral n-i-1) !! i | i <- [0..half]]
        where half = fromIntegral (n-1) `div` 2

-- | Stream of diagonal summands of pd
fibdiags :: [[Integer]]
fibdiags = map fibdiag [1..]

-- | Stream of fibonacci numbers, based on diagonal pd summands
fibspd :: [Integer]
fibspd = map sum fibdiags
