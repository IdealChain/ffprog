-- | Solution for FFP Aufgabe 2
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP2 where

-- | Stream of prime pairs (p, p+2)
pps :: [(Integer, Integer)]
pps = filter (\(x,y) -> y == x+2) $ zip primes (tail primes)

-- | Stream of primes (sieve of eratosthenes, from course handout)
primes = sieve [2..]
  where sieve (x:xs) = x : sieve [ y | y <- xs, mod y x > 0 ]

-- | Calculation of 2^n, slow/recursive version
pow :: Int -> Integer
pow 0 = 1
pow n = pow (n-1) + pow (n-1)

-- | Calculation of 2^n, faster version utilizing memoization
powFast :: Int -> Integer
powFast 0 = 1
powFast n = powList !! (n-1) + powList !! (n-1)
  where powList = [ powFast x | x <- [0..] ]


-- | Computing faculty
fac n = product [1..n]

-- | Caculation of g = sum n..k z^n/n!
-- | which is the Taylor Series for e^z

-- support function
h :: Int -> Int -> Float
h 0 _ = 0
h z i = (fromIntegral (z^i)) / (fromIntegral (fac i))

-- | naive implementation
f :: Int -> Int -> Float
f z 0 = 1
f z k = (h z k ) + (f z (k-1))

-- | implementation with memoization
fMT :: Int -> Int -> Float
fMT z 0 = 1
fMT z k = (fromIntegral (powKList !! k)) / (fromIntegral (facList !! k)) + fMT z (k-1)
  where powKList = [ z^x | x <- [0..] ];
        facList = [ fac x | x <- [0..]]

-- | compute Goedels Number for n
gz :: Integer -> Integer
gz n
  | n <= 0    = 0
  | otherwise = dmult (digits n) 0

-- | create list of digits of n starting with the lowest
digits :: Integer -> [Integer]
digits n
  | n >= 10   = digits (n `div` 10) ++ [ n `mod` 10]
  | otherwise = [n]

-- | calculate product of primes to the power of the digits
dmult :: [Integer] -> Int -> Integer
dmult [] _ = 1
dmult (d:ds) i = (primes !! i)^d * dmult ds (i+1)

-- | stream of Goedel Numbers starting from 1
gzs = [ gz x | x <- [1..] ]
