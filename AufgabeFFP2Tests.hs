-- | HUnit tests for FFP Aufgabe 2
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP2Tests where

import Test.HUnit
import AufgabeFFP2

ppsTests = TestLabel "pps" $ TestList [
  TestCase $ assertEqual "" (take 10 pps) ([(3,5),(5,7),(11,13),(17,19),(29,31),(41,43),(59,61),(71,73),(101,103),(107,109)]),
  TestCase $ assertEqual "" (pps!!20) ((347,349)),
  TestCase $ assertEqual "" (head(drop 30 pps)) (809,811)
  ]

powFastTests = TestLabel "powFast" $ TestList [
  TestCase $ assertEqual "" (map pow [0..16]) (map powFast [0..16])
  ]

fTests = TestLabel "f" $ TestList [
  TestCase $ assertEqual "" (f 0 0) (fMT 0 0),
  TestCase $ assertEqual "" (f 1 0) (fMT 1 0),
  TestCase $ assertEqual "" (f 0 1) (fMT 0 1),
  TestCase $ assertEqual "" (f 1 1) (fMT 1 1),
  TestCase $ assertEqual "" (f 2 3) (fMT 2 3),
  TestCase $ assertEqual "" (f 11 12) (fMT 11 12)
  ]

gzTests = TestLabel "gz" $ TestList [
  TestCase $ assertEqual "" (144) (gz 42),
  TestCase $ assertEqual "" (400) (gz 402),
  TestCase $ assertEqual "" (2) (gz 10),
  TestCase $ assertEqual "" (6) (gz 110),
  TestCase $ assertEqual "" (0) (gz 0),
  TestCase $ assertEqual "" (0) (gz (-1))
  ]

gzsTests = TestLabel "gzs" $ TestList [
  TestCase $ assertEqual "" ([2,4]) (take 2 gzs),
  TestCase $ assertEqual "" (144) (head (drop 41 gzs)),
  TestCase $ assertEqual "" (6) (head (drop 109 gzs)),
  TestCase $ assertEqual "" (400) (head (drop 401 gzs))
  ]

facTests = TestLabel "fac" $ TestList [
  TestCase $ assertEqual "" (fac 0) (1),
  TestCase $ assertEqual "" (fac 1) (1),
  TestCase $ assertEqual "" (fac 5) (120),
  TestCase $ assertEqual "" (fac 9) (362880)
  ]

hTests = TestLabel "h" $ TestList [
  TestCase $ assertEqual "" (0) (h 0 0),
  TestCase $ assertEqual "" (0) (h 0 1),
  TestCase $ assertEqual "" (1) (h 1 0),
  TestCase $ assertEqual "" (1) (h 1 1),
  TestCase $ assertEqual "" (2) (h 2 2),
  TestCase $ assertEqual "" (4.5) (h 3 3)
  ]

tests = TestList [ppsTests,
  powFastTests,
  fTests,
  gzTests,
  gzsTests,
  facTests,
  hTests
  ]
main = runTestTT tests
