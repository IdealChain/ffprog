-- | Solution for FFP Aufgabe 3
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP3 where

import Data.List

type Weight      = Int                 -- Gewicht
type Value       = Int                 -- Wert
type Item        = (Weight,Value)      -- Gegenstand als Gewichts-/Wertpaar
type Items       = [Item]              -- Menge der anfaenglich gegebenen Gegenstaende
type Load        = [Item]              -- Auswahl aus der Menge der anfaenglich gegebenen Gegenstaende; moegliche Rucksackbeladung, falls zulaessig
type Loads       = [Load]              -- Menge moeglicher Auswahlen
type LoadWghtVal = (Load,Weight,Value) -- Eine moegliche Auswahl mit Gesamtgewicht/-wert dieser Auswahl
type MaxWeight   = Weight              -- Hoechstzulaessiges Rucksackgewicht

-- | Generates all possible selections (combinations) of loads based on the available items
rs_generator :: Items -> Loads
rs_generator is = concatMap (\l -> combinations l is) [1..length is]
  where
    -- | Generates possible combinations of n items out of (length xs) items
    combinations :: Int -> [a] -> [[a]]
    combinations 0 _  = [ [] ]
    combinations n xs = [ y:ys | y:xs' <- tails xs, ys <- combinations (n-1) xs']

-- | Calculates weight and value for each load
rs_transformer :: Loads -> [LoadWghtVal]
rs_transformer ls = [ (l,w,v) | l <- ls, let (w,v) = sumWghtVal l ]
  where
    sumWghtVal l = foldl (\(w,v)(w',v') -> (w+w',v+v')) (0,0) l

-- | Filters invalid selections of loads (which are too heavy)
rs_filter :: MaxWeight -> [LoadWghtVal] -> [LoadWghtVal]
rs_filter max lws = filter (\(l,w,_) -> (not.null) l && w <= max) lws

-- | Selects the optimal loads, by highest value
rs_selector1 :: [LoadWghtVal] -> [LoadWghtVal]
rs_selector1 ls = filter (\(_,_,v) -> v == highestVal ls) ls
  where
    highestVal ls = maximum [ v | (_,_,v) <- ls ]

-- | Selects the optimal loads, by highest value, then by lowest weight
rs_selector2 :: [LoadWghtVal] -> [LoadWghtVal]
rs_selector2 ls = filter (\(_,w,_) -> w == lowestWght ls) highestVal
  where
    highestVal = rs_selector1 ls
    lowestWght ls = minimum [ w | (_,w,_) <- highestVal ]

-- | computes the binomial coefficient according to the lecture slides
binom :: (Integer, Integer) -> Integer
binom (n,k)
	| k==0 || n==k = 	1
	| otherwise =			binom (n-1, k-1) + binom (n-1, k)

-- | compute binom (n,k) via streams
binomS :: (Integer,Integer) -> Integer
binomS (n,k) = (pd !! fromInteger n) !! fromInteger k

-- | Stream of pascal tuples
pd :: [[Integer]]
pd = iterate nextRow [1]
   where nextRow row = zipWith (+) ([0] ++ row) (row ++ [0])

-- | compute binom (n,k) via memoization 
binomM :: (Integer,Integer) -> Integer
binomM (n,k) = (faclist !! (fromInteger n)) `div` (faclist !! fromInteger k) `div` (faclist !! fromInteger (n-k))

-- | memo table for faculty		
faclist = [fac x | x <- [0..]]
fac :: Integer -> Integer
fac 0 = 1
fac 1 = 1
fac n = n * (faclist !! fromInteger (n-1))
