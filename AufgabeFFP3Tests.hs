-- | HUnit tests for FFP Aufgabe 3
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP3Tests where

import Test.HUnit
import Control.Monad (unless)
import Data.List (sort)
import AufgabeFFP3

-- | Asserts that the list elements are equal to the expected elements, ignoring their order
assertListsEqual :: (Eq a, Show a, Ord a) => String -> [a] -> [a] -> Assertion
assertListsEqual preface expected actual =
  unless (sort actual == sort expected) (assertFailure msg)
 where msg = (if null preface then "" else preface ++ "\n") ++
             "expected: " ++ show expected ++ "\n but got: " ++ show actual

rsGeneratorTests = TestLabel "rs_generator" $ TestList [
  TestCase $ assertListsEqual "" [] $ rs_generator [],
  TestCase $ assertListsEqual "" [[(1,2)],[(3,4)],[(1,2),(3,4)]] $ rs_generator [(1,2),(3,4)],
  TestCase $ assertListsEqual "" [[(5,3)],[(2,7)],[(2,6)],[(10,100)],[(5,3),(2,7)],[(5,3),(2,6)],[(5,3),(10,100)],[(2,7),(2,6)],[(2,7),(10,100)],[(2,6),(10,100)],[(5,3),(2,7),(2,6)],[(5,3),(2,7),(10,100)],[(5,3),(2,6),(10,100)],[(2,7),(2,6),(10,100)],[(5,3),(2,7),(2,6),(10,100)]] $ rs_generator [(5,3),(2,7),(2,6),(10,100)]
  ]

rsTransformerTests = TestLabel "rs_transformer" $ TestList [
  TestCase $ assertListsEqual "" [] $ rs_transformer [],
  TestCase $ assertListsEqual "" [([(1,3)],1,3)] $ rs_transformer [[(1,3)]],
  TestCase $ assertListsEqual "" [([(1,3),(4,5)],5,8)] $ rs_transformer [[(1,3),(4,5)]],
  TestCase $ assertListsEqual "" [([(1,3),(4,5)],5,8),([(6,7)],6,7)] $ rs_transformer [[(1,3),(4,5)],[(6,7)]]
  ]

rsFilterTests = TestLabel "rs_filter" $ TestList [
  TestCase $ assertListsEqual "" [] $ rs_filter 1 [],
  TestCase $ assertListsEqual "" [] $ rs_filter 5 [([],1,3)],
  TestCase $ assertListsEqual "" [] $ rs_filter 0 [([(1,3)],1,3),([(1,3),(2,4)],3,7)],
  TestCase $ assertListsEqual "" [([(1,3)],1,3)] $ rs_filter 2 [([(1,3)],1,3),([(1,3),(2,4)],3,7)]
  ]

rsSelector1Tests = TestLabel "rs_selector1" $ TestList [
  -- selector1: by highest value
  TestCase $ assertListsEqual "" [] $ rs_selector1 [],
  TestCase $ assertListsEqual "" [([(1,3),(2,4)],3,7),([(1,3),(1,4)],2,7)] $ rs_selector1 [([(1,3)],1,3),([(1,3),(2,4)],3,7),([(1,3),(1,4)],2,7)],
  TestCase $ assertListsEqual "" [([(2,4)],2,4),([(1,2),(1,2)],2,4)] $ rs_selector1 [([(2,4)],2,4),([(1,2),(1,2)],2,4)]
  ]

rsSelector2Tests = TestLabel "rs_selector2" $ TestList [
  -- selector2: by highest value, then by lowest weight
  TestCase $ assertListsEqual "" [] $ rs_selector2 [],
  TestCase $ assertListsEqual "" [([(1,3),(1,4)],2,7)] $ rs_selector2 [([(1,3)],1,3),([(1,3),(2,4)],3,7),([(1,3),(1,4)],2,7)],
  TestCase $ assertListsEqual "" [([(2,4)],2,4),([(1,2),(1,2)],2,4)] $ rs_selector2 [([(2,4)],2,4),([(1,2),(1,2)],2,4)]
  ]

rsCompleteTests = TestLabel "rs" $ TestList [
  -- tests from assignment document
  TestCase $ assertListsEqual "" [([(2,7),(2,6)],4,13)] $
    (rs_selector1 . (rs_filter 5) . rs_transformer . rs_generator) [(5,3),(2,7),(2,6),(10,100)],
  TestCase $ assertListsEqual "" [([(2,7),(10,100)],12,107)] $
    (rs_selector1 . (rs_filter 13) . rs_transformer . rs_generator) [(5,3),(2,7),(2,6),(10,100)],
  TestCase $ assertListsEqual "" [] $
    (rs_selector1 . (rs_filter 1) . rs_transformer . rs_generator) [(5,3),(2,7),(2,6),(10,100)],
  TestCase $ assertListsEqual "" [([(2,7),(2,6)],4,13),([(5,13)],5,13)] $
    (rs_selector1 . (rs_filter 5) . rs_transformer . rs_generator) [(5,13),(2,7),(2,6),(10,100)],
  TestCase $ assertListsEqual "" [([(2,7),(2,6)],4,13)] $
    (rs_selector2 . (rs_filter 5) . rs_transformer . rs_generator) [(5,13),(2,7),(2,6),(10,100)]
  ]


binomTests = TestLabel "binom" $ TestList [
  TestCase $ assertEqual "" (binom (2,1)) (binomM (2,1)),
  TestCase $ assertEqual "" (binom (6,3)) (binomM (6,3)),
  TestCase $ assertEqual "" (binom (8,4)) (binomM (8,4)),
  TestCase $ assertEqual "" (binom (2,1)) (binomS (2,1)),
  TestCase $ assertEqual "" (binom (6,3)) (binomS (6,3)),
  TestCase $ assertEqual "" (binom (8,4)) (binomM (8,4))
  ]

rsTests = TestList [rsGeneratorTests, rsTransformerTests, rsFilterTests, rsSelector1Tests, rsSelector2Tests, rsCompleteTests]
nkTests = TestList [binomTests]
main = runTestTT $ TestList [rsTests, nkTests]
