-- | Solution for FFP Aufgabe 4
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP4 where

import Data.List
import Data.Ix
import Data.Array

type Weight      = Int                 -- Gewicht
type Value       = Int                 -- Wert
type MaxWeight   = Weight              -- Hoechstzulaessiges Rucksackgwicht

type Object      = (Weight,Value)      -- Gegenstand als Gewichts-/Wertpaar
type Objects     = [Object]            -- Menge der anfaenglich gegebenen Gegenstaende
type SolKnp      = [Object]            -- Auswahl aus der Menge der anfaenglich gegebenen Gegenstaende; moegliche Rucksackbeladung, falls zulaessig

-- | Node in Backtracking-Tree: value/weight of currently packed items,
--   max weight, remaining unpacked items and the packed items.
type NodeKnp     = (Value,Weight,MaxWeight,[Object],SolKnp)

-- | Finds the best knapsack solution for given objects and a maximum weight
knapsack :: Objects -> MaxWeight -> (SolKnp,Value)
knapsack objects limit = (bestSol,bestVal)
  where
    solutions = searchDfs succKnp goalKnp startNode
    startNode = (0,0,limit,objects,[])
    bestSol = head [ sol | (val,_,_,_,sol) <- solutions, val == bestVal ]
    bestVal = maximum [ val | (val,_,_,_,_) <- solutions ]

-- | Yields a list of valid successors of a node:
--   generate a successor for every remaining object
succKnp :: NodeKnp -> [NodeKnp]
succKnp (v,w,limit,objects,psol) = filter validKnp $ map takeObject objects
  where
    takeObject :: Object -> NodeKnp
    takeObject o@(w',v') = (v+v', w+w', limit, delete o objects, o:psol)
    validKnp :: NodeKnp -> Bool
    validKnp (_,w,limit,_,_) = w <= limit

-- | Determines if a node is a goal/solution node:
--   no further item can be added without exceeding the weight limit
goalKnp :: NodeKnp -> Bool
goalKnp (_,_,_,[],_)          = True
goalKnp (_,w,limit,objects,_) = not $ any (\(w',_) -> w + w' <= limit) objects


-- | Compute the binomial coefficient using dynamic programming
-- | and higher order functions
binomDyn :: (Integer,Integer) -> Integer
binomDyn (0,_) = 0
binomDyn (n,1) = n
binomDyn (n,k) = findTable t (n,k)
  where t = dynamic compB (bndsB (n,k))

compB :: Table Integer (Integer,Integer) -> (Integer,Integer) -> Integer
compB t (n,k)
  | k > 0 && k < n = (findTable t (n-1,k-1)) + (findTable t (n-1,k))
  | k == 0 || k == n = 1
  | otherwise = 0
   
bndsB :: (Integer,Integer) -> ((Integer,Integer),(Integer,Integer))
bndsB (n,k) = ((0,0),(n,k))

-- HELPER TYPES AND FUNCTIONS FROM COURSE HANDOUT --

-- | Backtracking Search Higher-Order-Function
searchDfs :: (Eq node) => (node -> [node]) -> (node -> Bool) -> node -> [node]
searchDfs succ goal x = (search' (push x emptyStack) )
  where
    search' s
      | stackEmpty s = []
      | goal (top s) = top s : search' (pop s)
      | otherwise
          = let x = top s
            in search' (foldr push (pop s) (succ x))

-- | Abstract Data Type: Stack
data Stack a = EmptyStk
               | Stk a (Stack a)

push :: a -> Stack a -> Stack a
push x s = Stk x s

pop :: Stack a -> Stack a
pop EmptyStk  = error "pop from an empty stack"
pop (Stk _ s) = s

top :: Stack a -> a
top EmptyStk  = error "top from an empty stack"
top (Stk x _) = x

emptyStack :: Stack a
emptyStack = EmptyStk

stackEmpty :: Stack a -> Bool
stackEmpty EmptyStk = True
stackEmpty _        = False

-- | Higher order implementation of Dynamic programming
-- | from the lecture slides

dynamic :: (Ix coord) => (Table entry coord -> coord -> entry) -> (coord, coord) -> (Table entry coord)
dynamic compute bnds = t
  where t = newTable (map (\coord -> (coord,compute t coord)) (range bnds))

-- | ADT table from the lecture slides
newtype Table a b = Tbl (Array b a)

newTable :: (Ix b) => [(b,a)] -> Table a b
newTable l = Tbl (array (lo,hi) l)
  where indices = map fst l
        lo      = minimum indices
        hi      = maximum indices

findTable (Tbl a) i = a ! i

updTable p@(i,x) (Tbl a) = Tbl (a // [p])
