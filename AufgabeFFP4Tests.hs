-- | HUnit tests for FFP Aufgabe 4
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP4Tests where

import Test.HUnit
import AufgabeFFP4

knapsackTests = TestLabel "knapsack" $ TestList [
  TestCase $ assertEqual "" ([],0) $ knapsack [] 0,
  TestCase $ assertEqual "" ([],0) $ knapsack [(5,3)] 4,
  TestCase $ assertEqual "" ([(5,3)],3) $ knapsack [(5,3)] 5,
  TestCase $ assertEqual "" ([(3,4),(3,4),(2,3),(2,3)],14) $ knapsack [(2,3),(2,3),(3,4),(3,4),(5,6)] 10
  ]

binomTests = TestLabel "binom" $ TestList [
  TestCase $ assertEqual "" 1  $ binomDyn (1,1),
  TestCase $ assertEqual "" 1  $ binomDyn (1,0),
  TestCase $ assertEqual "" 0  $ binomDyn (0,1),
  TestCase $ assertEqual "" 715  $ binomDyn (13,9),
  TestCase $ assertEqual "" 10  $ binomDyn (5,2)
  
  ]

main = runTestTT $ TestList [knapsackTests, binomTests]
