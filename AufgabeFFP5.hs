-- | Solution for FFP Aufgabe 5
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP5 where

import Data.Array
import Data.Ix
import Data.List
import Data.Monoid
import Data.Ord

-- | All possible section ranges of the array
sections :: (Ix a) => Array a b -> [(a,a)]
sections array = [ (i,j) | i <- ids, j <- ids, i <= j ]
  where ids = indices array

-- | All possible section ranges and their element sums
sectionSums :: (Ix a, Num b) => Array a b -> [((a,a),b)]
sectionSums a = [ (r, elementSum r) | r <- sections a ]
  where elementSum r = sum $ [ a ! x | x <- range r ]

-- | Maximum section sum value
mas :: Array Int Int -> Int
mas a = maximum $ map snd $ sectionSums a

-- | List of section ranges with maximum section sum values
--   Should be sorted implicitly by sections generation
amas :: Array Int Int -> [(Int,Int)]
amas a = [ r | (r,s) <- sectionSums a, s == mas a ]

-- | Longest section with maximum section sum value
lmas :: Array Int Int -> (Int,Int)
lmas a = maximumBy compareLength $ amas a
  where
    -- | Comparing monoid for maximum length, and then minimum fst
    compareLength = mconcat [comparing length, flip $ comparing fst]
    length (i,j) = j - i

-- minIndex :: (Ix a, Show a) => Array a b -> (b -> Bool) -> a
minIndex :: (Ix a, Show a) => Array a b -> (b -> Bool) -> a
minIndex arr wf = fst $ safe_minimum sol
  where sol =  divideAndConquer mi_indiv mi_solve mi_divide mi_combine lst
        lst = [ (i, wf (arr ! i)) | i <- range (bounds arr) ]
        

safe_minimum [] = error "No matching index"
safe_minimum xs = minimum xs

mi_indiv ls = length ls <= 1

mi_solve = id 

mi_divide (x:xs) = [[x], xs] 

mi_combine a [s1,s2] =  (filter (snd) s1) ++ (filter (snd) s2)

-- | HOF implementation of divide and conquer according to lecture
divideAndConquer :: (p -> Bool) -> (p -> s) -> (p -> [p]) -> (p -> [s] -> s) -> p -> s
divideAndConquer indiv solve divide combine initPb = dAC initPb
  where
    dAC pb
      | indiv pb = solve pb
      | otherwise = combine pb (map dAC (divide pb))


