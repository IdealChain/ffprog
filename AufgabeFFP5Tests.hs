-- | HUnit tests for FFP Aufgabe 5
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP5Tests where

import Test.HUnit
import Data.Array
import AufgabeFFP5

a :: Array Int Int
a = array (1,9) [(1,3),(2,(-5)),(3,0),(4,9),(5,2),(6,(-1)),(7,2),(8,(-5)),(9,1)]
b :: Array Int Int
b = array (1,9) [(1,3),(2,(-1)),(3,(-2)),(4,9),(5,2),(6,(-1)),(7,2),(8,0),(9,(-1))]
c :: Array Int Int
c = array (1,5) [(1,2),(2,3),(3,(-10)),(4,1),(5,4)]
z :: Array Int Int
z = array (1,3) [(1,1),(2,2),(3,3)]

masTests = TestLabel "mas" $ TestList [
  TestCase $ assertEqual "" [(1,1),(1,2),(1,3),(2,2),(2,3),(3,3)] $ sections z,
  TestCase $ assertEqual "" [((1,1),1),((1,2),3),((1,3),6),((2,2),2),((2,3),5),((3,3),3)] $ sectionSums z,
  TestCase $ assertEqual "" 12 $ mas a,
  TestCase $ assertEqual "" [(3,7),(4,7)] $ amas a,
  TestCase $ assertEqual "" [(1,7),(1,8),(4,7),(4,8)] $ amas b,
  TestCase $ assertEqual "" (3,7) $ lmas a,
  TestCase $ assertEqual "" (1,8) $ lmas b,
  TestCase $ assertEqual "" (1,2) $ lmas c
  ]

data Week = Mon | Tue | Wed | Thu | Fri | Sat | Sun deriving (Eq, Ord, Ix, Show)

d :: Array Week String
d = array (Tue, Sat) [(Wed, "work"), (Thu, "study"), (Tue, "study"), (Fri, "chill"), (Sat,"relax")]

minIndexTests = TestLabel "minIndex" $ TestList [
  TestCase $ assertEqual "" 4 $ minIndex a (>5),
  TestCase $ assertEqual "" 2 $ minIndex a (<0),
  TestCase $ assertEqual "" 3 $ minIndex a (even),
  TestCase $ assertEqual "" 1 $ minIndex b (odd),
  -- TestCase $ assertFailure "" "no matching ndex" $ minIndex a (>5),
  TestCase $ assertEqual "" Sat $ minIndex d (=="relax"),
  TestCase $ assertEqual "" Wed $ minIndex d (=="work"),
  TestCase $ assertEqual "" Fri $ minIndex d (=="chill"),
  --TestCase $ assertFailure "" "No matching index" $ minIndex d (=="swim"),
  TestCase $ assertEqual "" Tue $ minIndex d (/="chill")
  ]

main = runTestTT $ TestList [masTests, minIndexTests]
