-- | Solution for FFP Aufgabe 6
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP6 where

import Data.Array
import Data.Ix
import Data.List
import Debug.Trace

data Color     = Black | White deriving (Show, Eq)
data Digit     = One | Two | Three | Four | Five | Six
                 | Seven | Eight | Nine | Blank deriving (Show, Eq, Ord, Enum)
type Str8ts    = Array (Int,Int) (Color,Digit)
type ColorOut  = Char -- Nur 'b' fuer schwarz und 'w' fuer weiss werden benutzt.
type DigitOut  = Int -- Nur 1,2,3,4,5,6,7,8,9,0 werden benutzt;
                     -- 0 wird dabei als Platzhalter fuer 'blank' benutzt.
type Str8tsOut = Array (Int,Int) (ColorOut,DigitOut)

type Matrix a  = [Row a]
type Row a     = [a]
type Grid      = Matrix (Color,Digit)
type Choices   = [(Color,Digit)]

start :: (Int,Int)
start = (1,1)
end :: (Int,Int)
end = (9,9)

-- | Converts array to grid format
str8tsToGrid :: Str8ts -> Grid
str8tsToGrid s = [[(s!(x,y)) | y <- [1..9]] | x <- [1..9]]

-- | Converts grid to array format
gridToStr8ts :: Grid -> Str8ts
gridToStr8ts s = array (start,end) $ zipWith pair (range (start,end)) $ concat s
  where pair a b = (a,b)

-- | Creates an empty Str8ts
emptyStr8ts :: Str8ts
emptyStr8ts = gridToStr8ts $ repeat $ replicate 9 (White,Blank)

-- | Convert Str8ts to Str8tsOut for creating the needed output format
convertOut :: Str8ts -> Str8tsOut
convertOut s = array (start,end) [(i,convert (s!i)) | i <- range (start,end)]
  where
    convert :: (Color,Digit) -> (ColorOut,DigitOut)
    convert (c,d) = (convertColor c, convertDigit d)

-- | Convert Color to ColorOut
convertColor :: Color -> ColorOut
convertColor Black = 'b'
convertColor White = 'w'

-- | Convert Digit to DigitOut
convertDigit :: Digit -> DigitOut
convertDigit One   = 1
convertDigit Two   = 2
convertDigit Three = 3
convertDigit Four  = 4
convertDigit Five  = 5
convertDigit Six   = 6
convertDigit Seven = 7
convertDigit Eight = 8
convertDigit Nine  = 9
convertDigit Blank = 0

-- | Print a Str8ts for debugging
printStr8ts :: Str8tsOut -> IO ()
printStr8ts s = do putStrLn (str8tsToString s)
  where
    str8tsToString :: Str8tsOut -> String
    str8tsToString a = unlines [unwords [cell (a ! (x,y)) | y <- [1..9]] | x <- [1..9]]
    cell :: (ColorOut,DigitOut) -> String
    cell ('b',0) = "[ ]"
    cell ('b',d) = "[" ++ show d ++ "]"
    cell ('w',0) = "   "
    cell ('w',d) = " " ++ show d ++ " "

-- | Construct all choices for the blank entries in the grid
choices :: Grid -> Matrix Choices
choices = map (map choice)
  where
    choice :: (Color,Digit) -> Choices
    choice (White,Blank) = [(White,d) | d <- [One .. Nine]]
    choice (c,d)         = [(c,d)]

-- | Compute all grids that arise from making every possible choice
expand :: Matrix Choices -> [Grid]
expand = cp . map cp
  where
    -- | Cartesian product according to lecture
    cp :: [[a]] -> [[a]]
    cp [] = [[]]
    cp (xs:xss) = [x:ys | x <- xs, ys <- cp xss]

-- | Filter and select only valid grids
valid :: Grid -> Bool
valid g = all straights (rows g) && all straights (cols g) &&
          all (nodups.digits) (rows g) && all (nodups.digits) (cols g)
  where
    rows = id
    cols = transpose

-- | Returns only the non-blank digits of a list
digits :: [(Color,Digit)] -> [Digit]
digits = filter (\d -> d /= Blank) . map snd

 -- | Whether the given list contains no duplicates
nodups :: Eq a => [a] -> Bool
nodups [] = True
nodups (x:xs) = all (x/=) xs && nodups xs

-- | Map a function over fields (groups of same-colored cells)
mapFields :: ((Color,[a]) -> b) -> (a -> Color) -> [a] -> [b]
mapFields _ _ []  = []
mapFields f fc cs = f (color,field) : mapFields f fc rest
  where
    color = (fc.head) cs
    (field, rest) = span (\c -> fc c == color) cs

-- | Whether all fields in a row are straights
straights :: [(Color,Digit)] -> Bool
straights [] = True
straights cs = all (==True) $ mapFields fieldStraight fst cs
  where
    -- | Whether a white field of cells is a straight
    fieldStraight :: (Color,[(Color,Digit)]) -> Bool
    fieldStraight (Black,_)  = True
    fieldStraight (White,[]) = True
    fieldStraight (White,cs) = s == [head s .. last s]
      where s = (sort . map (convertDigit.snd)) cs

-- | Whether a row of cells not violates the straight constraint
notViolateStraights :: [(Color,Digit)] -> Bool
notViolateStraights [] = True
notViolateStraights cs = all (==True) $ mapFields notViolateStraight fst cs
  where
    -- | Whether a white field of cells not violates the straight constraint
    notViolateStraight :: (Color,[(Color,Digit)]) -> Bool
    notViolateStraight (Black,_)  = True
    notViolateStraight (White,[]) = True
    notViolateStraight (White,cs) = all (withinLimits l) ds
      where
        ds = map snd cs
        l = straightLimits ds

-- | Remove choices which would violate the straight constraint
pruneStraights :: Row Choices -> Row Choices
pruneStraights row = concat $ mapFields pruneStraight (fst.head) row
  where
    pruneStraight :: (Color,Row Choices) -> Row Choices
    pruneStraight (Black,fs) = fs
    pruneStraight (White,fs) = [pruneChoice l f | f <- fs]
      where
        l = straightLimits $ map snd $ singletonBlanks fs
        pruneChoice :: (Int,Int) -> Choices -> Choices
        pruneChoice _ [c]    = [c]
        pruneChoice (1,9) cs = cs
        pruneChoice l (c:cs) = filter (\(c,d) -> withinLimits l d) (c:cs)

-- | Upper/lower limits which a valid straight must not exceed
straightLimits :: [Digit] -> (Int,Int)
straightLimits ds = (lower,upper)
  where
    nonblank = filter (/=0) $ map convertDigit ds
    fields   = length ds - 1
    upper
      | null nonblank = 9
      | otherwise     = minimum [9, (minimum nonblank) + fields]
    lower
      | null nonblank = 1
      | otherwise     = maximum [1, (maximum nonblank) - fields]

-- | Whether a given digit is within valid straight limits
withinLimits :: (Int,Int) -> Digit -> Bool
withinLimits _ Blank         = True
withinLimits (lower,upper) d = lower <= number && number <= upper
  where number = convertDigit d

-- | returns fixed singletons and blank entries for multiple choices
singletonBlanks :: Row Choices -> Row (Color,Digit)
singletonBlanks [] = []
singletonBlanks (c:cs)
  | length c == 1 = (head c : singletonBlanks cs)
  | otherwise     = ((White,Blank) : singletonBlanks cs)
  
-- | Applies a grid solver to a Str8ts riddle
solveAsGrid :: (Grid -> [Grid]) -> Str8ts -> [Str8ts]
solveAsGrid solver = (map gridToStr8ts) . solver . str8tsToGrid

-- | Remove any choices from a cell that occurs as a (fixed) singleton
--   entry in the same row, column.
--   TODO maybe: prune choices which would validate the straight constraint
prune :: Matrix Choices -> Matrix Choices
prune = pruneBy cols . pruneBy rows
  where
    rows = id
    cols = transpose
    -- | Prune invalid choices from a grid using a transformation function
    pruneBy :: ([Row Choices] -> [Row Choices]) -> [Row Choices] -> [Row Choices]
    pruneBy f = f . map pruneRow . f
    -- | Remove choices from any choice that is not fixed
    pruneRow :: Row Choices -> Row Choices
    pruneRow row = map (remove fixed) row -- $ pruneStraights row
      where fixed = digits $ [c | [c] <- row]
    -- | Remove given choices from any choice that is not fixed
    remove :: [Digit] -> Choices -> Choices
    remove ds [c]    = [c]
    remove ds (c:cs) = filter (\(c,d) -> not $ elem d ds) (c:cs)
    
-- | Naive (inefficient) Str8ts solver
naiveStr8ts :: Str8ts -> Str8tsOut
naiveStr8ts s
  | null solutions = convertOut s
  | otherwise      = convertOut $ head $ solutions
  where
    solve     = solveAsGrid $ filter valid . expand . prune . choices
    solutions = solve s

-- | Fast (more efficient) Str8ts solver
-- | fastStr8ts :: Str8ts -> Str8tsOut
fastStr8ts :: Str8ts -> Str8tsOut
fastStr8ts s
  | null solutions = convertOut s
  | otherwise      = convertOut $ head $ solutions
  where
    solve     = solveAsGrid $ search . choices
    solutions = solve s

-- | generate expansion using the element
-- | with least choices
expandOne :: Matrix Choices -> [Matrix Choices]
expandOne rows = [rows1 ++ [row1 ++ [c] : row2] ++ rows2 | c <- cs]
  where
    (rows1, row:rows2)  = break (any smallest) rows
    (row1, cs:row2)     = break smallest row
    smallest cs         = length cs == n
    n                   = minimum (counts rows)
    counts              = filter (/=1) . map length . concat

-- | check if matrix is correct
--   * no singleton choices contain duplicates
--   * no singleton choices violate the straights constraint
safe :: Matrix Choices -> Bool
safe m = all ok (rows m) && all ok (cols m)
  where
    ok row = (nodups.digits) (singletons row) && notViolateStraights (singletonBlanks row) && notEmptyChoice row
    singletons row = [c | [c] <- row]
    notEmptyChoice row = not $ any (\cs -> null cs) row
    rows = id
    cols = transpose

-- | check if problem is solved (all choices are singletons)
complete :: Matrix Choices -> Bool
complete = all (all single)
  where single cs = 1 == length cs

-- | search according to lecture
search :: Matrix Choices -> Matrix Choices
search m
  | not (safe m)  = []
  | complete m'   = [map (map head) m']
  | otherwise     = concat (map search (expandOne m'))
  where m' = prune m
