-- | HUnit tests for FFP Aufgabe 6
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP6Tests where

import Test.HUnit
import Data.Array
import AufgabeFFP6

choicesTests = TestLabel "choices" $ TestList [
  TestCase $ assertEqual "" [[[(Black,Blank)]]] $ choices [[(Black,Blank)]],
  TestCase $ assertEqual "" [[[(Black,Six)]]] $ choices [[(Black,Six)]],
  TestCase $ assertEqual "" [[[(White,Six)]]] $ choices [[(White,Six)]],
  TestCase $ assertEqual "" [[[(White,One),(White,Two),(White,Three),(White,Four),(White,Five),(White,Six),(White,Seven),(White,Eight),(White,Nine)]]] $ choices [[(White,Blank)]]
  ]

isStraightTests = TestLabel "isStraight" $ TestList [
  TestCase $ assertEqual "" True $ straights [],
  TestCase $ assertEqual "" True $ straights [(Black,One)],
  TestCase $ assertEqual "" True $ straights [(White,One)],
  TestCase $ assertEqual "" True $ straights [(White,One),(Black,Blank),(White,Four)],
  TestCase $ assertEqual "" True $ straights [(White,One),(Black,Blank),(White,Two),(White,Four),(White,Three)],
  TestCase $ assertEqual "" False $ straights [(White,One),(Black,Blank),(White,Two),(White,Four),(White,One)]
  ]

-- | Prints the Str8ts to solve and the first derived solution
printSolve :: Str8ts -> (Str8ts -> Str8tsOut) -> String -> IO ()
printSolve s solver name = do
  putStrLn ("Solving using " ++ name ++ ":")
  printStr8ts (convertOut s)
  putStrLn "Solution:"
  (printStr8ts . solver) s

-- | Completely solved Str8ts riddle from assignment sheet
a :: Str8ts
a = gridToStr8ts $ [
  [(Black,Blank),(Black,Blank),(White,Eight),(White,Nine),(Black,Blank),(White,One),(White,Two),(Black,Blank),(Black,Blank)],
  [(White,Five),(White,Eight),(White,Seven),(White,Six),(White,Nine),(White,Two),(White,Three),(White,Four),(White,One)],
  [(White,Seven),(White,Nine),(White,Six),(White,Eight),(White,Five),(Black,Blank),(White,Four),(White,Three),(White,Two)],
  [(White,Six),(Black,Seven),(Black,Nine),(White,Five),(White,Four),(Black,Blank),(White,One),(White,Two),(White,Three)],
  [(Black,Blank),(White,Four),(White,Two),(White,Seven),(White,Three),(White,Six),(White,Five),(White,One),(Black,Eight)],
  [(White,Four),(White,Three),(White,Five),(Black,Two),(White,Eight),(White,Seven),(Black,Blank),(Black,Blank),(White,Six)],
  [(White,Two),(White,One),(White,Three),(Black,Blank),(White,Six),(White,Five),(White,Seven),(White,Eight),(White,Four)],
  [(White,Three),(White,Two),(White,One),(White,Four),(White,Seven),(White,Eight),(White,Six),(White,Nine),(White,Five)],
  [(Black,One),(Black,Blank),(White,Four),(White,Three),(Black,Blank),(White,Nine),(White,Eight),(Black,Six),(Black,Blank)]
  ]

-- | Unsolved Str8ts riddle from assignment sheet
b :: Str8ts
b = emptyStr8ts // [
  ((1,1),(Black,Five)), ((1,4),(White,Four)), ((1,5),(Black,Blank)), ((1,6),(Black,Blank)), ((1,9),(Black,Seven)),
  ((2,1),(Black,Blank)), ((2,4),(Black,Blank)), ((2,9),(Black,Blank)),
  ((3,3),(Black,Nine)), ((3,4),(Black,Three)),
  ((4,1),(White,Two)), ((4,6),(Black,Six)), ((4,7),(Black,Blank)),
  ((5,1),(Black,Blank)), ((5,9),(Black,One)),
  ((6,1),(White,Seven)), ((6,3),(Black,Blank)), ((6,4),(Black,Blank)), ((6,8),(White,Three)),
  ((7,6),(Black,Blank)), ((7,7),(Black,Blank)),
  ((8,1),(Black,Four)), ((8,5),(White,Nine)), ((8,6),(Black,Blank)), ((8,9),(Black,Blank)),
  ((9,1),(Black,Blank)), ((9,2),(White,Six)), ((9,4),(Black,Blank)), ((9,5),(Black,Blank)), ((9,7),(White,One)), ((9,9),(Black,Blank))
  ]

-- | 'a' with ten entries missing (to be determined)
c :: Str8ts
c = a // [(i,(White,Blank)) | i <- [(2,7),(2,8),(3,8),(4,4),(4,5),(6,5),(6,6),(7,1),(8,1),(8,2)]]

-- | Unsolveable / invalid riddle
d :: Str8ts
d = emptyStr8ts // [ ((1,1),(White,One)), ((1,9),(White,One)) ]

solvableTests = TestLabel "solvable" $ TestList [
  TestCase $ assertEqual "" (convertOut a) $ (naiveStr8ts a),
  TestCase $ assertEqual "" (convertOut a) $ (fastStr8ts a),
  TestCase $ assertEqual "" (convertOut d) $ (fastStr8ts d)
  ]

main = do
  runTestTT $ TestList [choicesTests, isStraightTests, solvableTests]
  printSolve c naiveStr8ts "naiveStr8ts"
  printSolve c fastStr8ts "fastStr8ts"
  printSolve b fastStr8ts "fastStr8ts"
