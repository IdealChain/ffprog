-- | Solution for FFP Aufgabe 7
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)
module AufgabeFFP7 where

import Data.List

type TargetValue = Integer
type Digit = Integer -- only from 1..9
type Digits = [Digit]
type Number = Integer

digits = [1..9] :: Digits

data Operator = P | T deriving (Eq, Show) -- P for plus, t for times
data Expr = Opd Number | Opr Operator Expr Expr deriving (Eq, Show)

mkTV :: Digits -> TargetValue -> [Expr]
mkTV ds tv = map (convertOut ds) (solve ds tv)


data Choice   = Plus | Times | Blank deriving (Eq, Show) -- | p for plus, t for times, Blank for combining digits
type Choices  = [Choice]

-- | convert to output format according to the description
convertOut :: Digits -> Choices -> Expr
convertOut ds cs = choicesToExpr ds_nb cs_nb
  where
    ds_nb = (reduce cs ds (\x y -> x*10+y) Blank)
    cs_nb = filter (\x -> x /= Blank) cs

choicesToExpr :: Digits -> Choices -> Expr
choicesToExpr (d:ds) (c:cs)
  | c == Plus   = Opr P (Opd d) (choicesToExpr ds cs)
  | otherwise   = Opr T (Opd d) (choicesToExpr ds cs)

choicesToExpr (d:ds) [] = Opd d


-- | naive exhausive search for solution
solve :: Digits -> TargetValue -> [Choices]
solve ds tv = filter (valid ds tv) ((expand  . choices) ds)

-- | check if value equals targetValue
valid :: Digits -> TargetValue -> Choices -> Bool
valid ds tv cs = (tv == (value ds cs))

-- | list of potential operations
choices :: Digits -> [Choices]
choices = (map (\x -> [Plus, Times, Blank])) . tail

-- | expand every choice with an additional choice
expand :: [Choices] -> [Choices]
expand = cp

-- | cartesian product with iteself
cp :: [[a]] -> [[a]]
cp []  = [[]]
cp (xs:xss) = [ x:ys | x <- xs, ys <- cp xss ]

-- | calculate sum of the sequence
-- | reduce blanks, then times and fold to sum
value :: Digits -> Choices -> Integer
value ds cs = foldl (+) 0 (reduce cs_nb ds_nb (*) Times)
  where 
    ds_nb = reduce cs ds (\x y -> x*10+y) Blank
    cs_nb = filter (\x -> x /= Blank) cs
    cs_nt = filter (\x -> x /= Times) cs_nb

-- | reduce operands by applying f
reduce:: Choices -> [Integer] -> (Integer -> Integer -> Integer)-> Choice -> [Integer]
reduce [] xs f ch = xs
reduce (c:cs) (x:y:is) f ch
  | c == ch  = reduce cs (z:is) f ch
  | otherwise   = x: reduce cs (y:is) f ch
  where z = f x y
