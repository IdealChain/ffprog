-- | HUnit tests for FFP Aufgabe 7
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP7Tests where

import Test.HUnit
import AufgabeFFP7

sol1 = Opr P (Opd 12) (Opr P (Opd 34) (Opr T (Opd 5) (Opr P (Opd 6) (Opr P (Opd 7) (Opr P (Opd 8) (Opd 9))))))
sol2 = Opr P (Opd 1) (Opr T (Opd 2) (Opr P (Opd 3) (Opr P (Opd 4) (Opr P (Opd 5) (Opr P (Opd 67) (Opr P (Opd 8) (Opd 9)))))))

mkTVTests = TestLabel "minIndex" $ TestList [
  TestCase $ assertEqual "" [Opr T (Opd 1) (Opd 2)] $ mkTV [1..2] 2,
  TestCase $ assertEqual "" [Opr P (Opd 1) (Opd 2)] $ mkTV [1..2] 3,
  TestCase $ assertEqual "" True $ elem sol1 $ mkTV [1..9] 100,
  TestCase $ assertEqual "" True $ elem sol2 $ mkTV [1..9] 100
  ]

main = runTestTT $ TestList [mkTVTests]
