-- | Solution for FFP Aufgabe 8
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

{-# LANGUAGE FlexibleInstances #-}

module AufgabeFFP8 where

import Data.Maybe
import Data.List
import Data.Array

type Op = (Int->Int->Int)

-- | Make Op an instance of Show (printing) and of Eq to make them comparable.
--   This works reliably only for the known operators.
instance Show Op where
  show op
    | op 3 3 == 6 = "plus"
    | op 3 3 == 0 = "minus"
    | op 3 3 == 9 = "times"
    | op 3 3 == 1 = "div"
    | otherwise   = "unknown operator"

instance Eq Op where
  x == y = (x 3 3 == y 3 3)

-- | Evaluates a sequence of numbers and a sequence of operators
--   to calculate a resulting number.
eval :: Array Int Int -> Array Int Op -> Int
eval num op = fromMaybe (error "Division by 0") (num `safeEval` op)

-- | Version of 'eval' safe for divion by zero errors.
--   Recursive folding from the left.
safeEval :: Array Int Int -> Array Int Op -> Maybe Int
safeEval num op = (elems num) `safeEval'` (elems op)

safeEval' :: [Int] -> [Op] -> Maybe Int
safeEval' []  _ = Just 0
safeEval' [s] _ = Just s
safeEval' (s:num:nums) (op:ops)
  | num == 0 && op == div = Nothing
  | otherwise             = ((s `op` num):nums) `safeEval'` ops



-- | Returns, applied on an sequence of numbers and a target result
--   number, possible sequences of operators to yield that result.
--   This variant uses the generate/transform/filter principle.
yield_gtf :: Array Int Int -> Int -> [Array Int Op]
yield_gtf num target = (filt target) $ (transform num) $ (generate l [(+),(-),(*),div])
  where l = snd (bounds num) - fst (bounds num)

-- | Generates all possible combinations of operations
generate :: Int -> [Op] -> [Array Int Op]
generate l ops = map (listArray (1,l)) $ generate' l ops

generate' :: Int -> [Op] -> [[Op]]
generate' 0 ops = [[]]
generate' l ops = [ (op:tail) | op <- ops, tail <- generate' (l-1) ops ]

-- | Calculates the resulting number for every given operation sequence
transform :: Array Int Int -> [Array Int Op] -> [(Array Int Op, Maybe Int)]
transform num ops = [ (op, num `safeEval` op) | op <- ops]

-- | Filters out all invalid results
filt :: Int -> [(Array Int Op, Maybe Int)] -> [Array Int Op]
filt target ops = [ op | (op,t) <- ops, maybe False (==target) t ]



-- | Returns, applied on an sequence of numbers and a target result
-- | number, possible sequences of operators to yield that result.
-- | This variant uses the backtracking principle.
yield_bt :: Array Int Int -> Int -> [Array Int Op]
yield_bt nums target = map (\l -> listArray (1, length l) l) result
  where
    result  = searchDfs succ goal []
    n       = (snd $ bounds nums) - 1
    succ    = succ_bt n
    goal    = goal_bt nums target

type Node = [Op]

-- | generate the next node
-- | append all possible oeprators to the current node
succ_bt :: Int -> Node -> [Node]
succ_bt max ops
  | max <= length ops   = [] 
  | otherwise           = [ ops ++ [o] | o <- [(+),(-),(*),div] ]

-- | test if node is a valid solution
-- | only true if the number of operators is valid and
-- | the current value equals the target value
goal_bt :: Array Int Int -> Int -> Node -> Bool
goal_bt nums targetValue node
  | n /= target_length  = False
  | otherwise           = targetValue == (fromMaybe (targetValue-1) ev_value)
  where 
    i_node        = listArray (1,n) node
    target_length = (snd $ bounds nums) - 1
    n             = length node
    ev_value      = safeEval' (elems nums) node

-- | DFS from lecture slides
searchDfs :: (Eq node) => (node -> [node]) -> (node -> Bool) -> node -> [node]
searchDfs succ goal x = (search' (push x emptyStack))
  where
    search' s
      | stackEmpty s = []
      | goal (top s) = top s : search' (pop s)
      | otherwise
          = let x = top s
            in search' (foldr push (pop s) (succ x))

-- | ADT Stack from the lecture
data Stack a = EmptyStk
                | Stk a (Stack a)

push x s = Stk x s

pop EmptyStk  = error "pop from empty stack"
pop (Stk _ s) = s

top EmptyStk = error "top from emty stack"
top (Stk x _) = x

emptyStack = EmptyStk

stackEmpty EmptyStk = True
stackEmpty _        = False
