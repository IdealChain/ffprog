-- | HUnit tests for FFP Aufgabe 8
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP8Tests where

import Test.HUnit
import Data.Array
import AufgabeFFP8

evalTests = TestLabel "eval" $ TestList [
  TestCase $ assertEqual "" 0 $ eval (array (1,3) [(1,1),(2,2),(3,3)]) (array (1,2) [(1,(+)),(2,(-))]),
  TestCase $ assertEqual "" 5 $ eval (array (1,3) [(1,1),(2,2),(3,3)]) (array (1,2) [(1,(*)),(2,(+))]),
  TestCase $ assertEqual "" (-3) $ eval (array (1,3) [(1,1),(2,2),(3,3)]) (array (1,2) [(1,(-)),(2,(*))]),
  TestCase $ assertEqual "" (Just 0) $ safeEval (array (1,2) [(1,0),(2,1)]) (array (1,1) [(1,div)]),
  TestCase $ assertEqual "" (Just 1) $ safeEval (array (1,2) [(1,1),(2,1)]) (array (1,1) [(1,div)]),
  TestCase $ assertEqual "" Nothing $ safeEval (array (1,2) [(1,1),(2,0)]) (array (1,1) [(1,div)])
  ]

eval_yield :: (Array Int Int -> Int -> [Array Int Op]) -> Array Int Int -> Int -> Int -> Bool
eval_yield yield num target variants = variants_correct && target_correct
  where
    results = yield num target
    target_correct = all (\ops -> eval num ops == target) results
    variants_correct = length results == variants

yieldgtfTests = TestLabel "yield_gtf" $ TestList [
  TestCase $ assertBool "" $ eval_yield yield_gtf (array (1,3) [(1,1),(2,2),(3,3)]) 6 2,
  TestCase $ assertBool "" $ eval_yield yield_gtf (array (1,3) [(1,1),(2,2),(3,3)]) 4 0,
  TestCase $ assertBool "" $ eval_yield yield_gtf (array (1,3) [(1,1),(2,2),(3,3)]) 0 4,
  TestCase $ assertBool "" $ eval_yield yield_gtf (array (1,3) [(1,1),(2,2),(3,0)]) 0 6
  ]

yieldbtTests = TestLabel "yield_bt" $ TestList [
  TestCase $ assertBool "" $ eval_yield yield_bt (array (1,3) [(1,1),(2,2),(3,3)]) 6 2,
  TestCase $ assertBool "" $ eval_yield yield_bt (array (1,3) [(1,1),(2,2),(3,3)]) 4 0,
  TestCase $ assertBool "" $ eval_yield yield_bt (array (1,3) [(1,1),(2,2),(3,3)]) 0 4,
  TestCase $ assertBool "" $ eval_yield yield_bt (array (1,3) [(1,1),(2,2),(3,0)]) 0 6
  ]

main = runTestTT $ TestList [evalTests, yieldgtfTests, yieldbtTests]
