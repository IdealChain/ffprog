-- | Solution for FFP Aufgabe 9
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP9 where

import Data.List
import Data.Array
import Test.QuickCheck

type Text  = String
type Word  = String
type First = Int
type Last  = Int

-- | Naive string search function advancing one character at a time
occS :: Text -> Word -> [(First,Last)]
occS text word = filterS text word $ genS (length text) (length word)

genS :: Int -> Int -> [(First,Last)]
genS _ 0 = []
genS text word
  | text < word = []
  | otherwise   = [ (i,i+word-1) | i <- [0..(text-word)] ]

filterS :: Text -> Word -> [(First,Last)] -> [(First,Last)]
filterS text word occs = [ occ | occ <- occs, rword `isPrefixOf` (rcut occ) ]
  where
    rword      = reverse word
    rcut (f,l) = reverse $ take (l+1) text



-- | Boyer-Moore-Horspool improved string search function advancing
--   multiple characters if possible
occI :: Text -> Word -> [(First,Last)]
occI _ [] = []
occI text word = findOcc ta wa
  where
    ta = listArray (0,(length text)-1) text
    wa = listArray (0,(length word)-1) word

findOcc text word = occ' 0 m
    where
      (_,m) = bounds word
      (_,n) = bounds text
      occ' i k
        -- | end recursion
        | i+m > n   = []
        -- | entire pattern matched
        -- | shift entire pattern length
        | k == 0    = [ (i,i+m) | text!i == word!0 ] ++ occ' (i+m+1) m
        -- | shift 1 position
        | text!(i+k) == word!k  = occ' i (k-1)
        -- | shift according to table
        | otherwise             = occ' (i+shiftDistance!(text!(i+m))) m
      
      -- | shift table for the entire ASCII range
      shiftDistance :: Array Char Int
      shiftDistance = accumArray min (m+1) (toEnum 0, toEnum 255) [ (word!k, m-k) | k <- [0..m-1] ]


-- | QuickCheck property for testing algorithm equivalence
prop_coincide :: Text -> Word -> Bool
prop_coincide text word = occS text word == occI text word
