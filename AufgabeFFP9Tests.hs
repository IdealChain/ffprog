-- | HUnit tests for FFP Aufgabe 9
--
--   Daniel Achleitner (0926807), Xiaolin Zhang (0825548)

module AufgabeFFP9Tests where

import Test.HUnit
import Test.QuickCheck
import AufgabeFFP9

occTests occ name = TestLabel name $ TestList [
  TestCase $ assertEqual "" [] $ (occ "" ""),
  TestCase $ assertEqual "" [] $ (occ "a" ""),
  TestCase $ assertEqual "" [] $ (occ "" "a"),
  TestCase $ assertEqual "" [(0,0)] $ (occ "a" "a"),
  TestCase $ assertEqual "" [(2,3),(4,5),(6,7)] $ (occ "bananana" "na")
  ]

main = do
  runTestTT $ TestList [occTests occS "occS", occTests occI "occI"]
  quickCheck prop_coincide
